import * as cluster from 'cluster';
import { cpus } from 'os';

import { start } from './infrastructure/server';
import MongoDBConnection from './infrastructure/database/MongoDBConnection';


if (cluster.isMaster) {
    cluster.on('fork', (worker): void => {
        console.log('debug', `Worker ${worker.id} online`);
    });
    cluster.on('exit', (worker): void => {
        console.log('warn', `Worker ${worker.id} exited`);
        cluster.fork();
    });

    cpus().forEach((): void => {
        cluster.fork();
    });
} else {
    MongoDBConnection.connect(start);
}
