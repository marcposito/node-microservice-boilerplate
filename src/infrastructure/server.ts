import * as express from 'express';

import config from "./config";


export const app = express();

export const start = (): void => {
    app.listen(config.port, (): void => {
        console.log(`Microservice running on port ${config.port}`);
    });
};
