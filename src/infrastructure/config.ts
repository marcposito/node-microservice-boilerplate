const config = {
    port: 5800,
    mongo: {
        uri: 'mongodb://localhost:27017',
        database: 'default'
    }
};

export default config;
