export default interface IRepository {
    find(query: any): Promise<any>;
    insert(item: any): Promise<any>;
    update(id: string, item: any): Promise<any>;
    delete(id: string): Promise<boolean>;
}
