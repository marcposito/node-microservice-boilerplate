import MongoDBConnection from '../database/MongoDBConnection';
import IRepository from "./IRepository";


class MongoDBRepository implements  IRepository {

    private readonly _collection: string;

    public constructor(collection: string) {
        this._collection = collection;
    }

    async find(query: any = {}): Promise<any> {
        const data = await this.collection.find(query);
        return data;
    }

    async insert(item: any): Promise<any> {
        const data = await this.collection.insertOne(Object.assign(item, {
            updatedAt: new Date(),
            createdAt: new Date()
        }));

        return data.ops;
    }

    async update(id: string, item: any): Promise<any> {
        const data = await this.collection.findOneAndUpdate({ _id: id }, {
            $set: Object.assign(item, {
                updatedAt: new Date()
            })
        }, { returnOriginal: false });

        return data.value;
    }

    async delete(id: string): Promise<boolean> {
        const data = await this.collection.deleteOne({ _id: id });

        return data.result.ok === 1;
    }

    get collection() {
        return this.connection.collection(this._collection);
    }

    get connection() {
        return MongoDBConnection.getConnection();
    }
}

export default MongoDBRepository;
