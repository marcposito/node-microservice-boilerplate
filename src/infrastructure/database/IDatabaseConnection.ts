import { Db } from "mongodb";


export default interface IDatabaseConnection {
    connect(uri: string, database: string, options: any ): Promise<void>;
    isConnected(): boolean;
    getConnection(): Db;
}
