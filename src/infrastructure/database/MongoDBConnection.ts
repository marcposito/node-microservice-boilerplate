import { MongoClient, Db } from 'mongodb';

import IDatabaseConnection from "./IDatabaseConnection";
import config from "../config";


class MongoDBConnection implements IDatabaseConnection {

    callbacks: { [key: string]: Array<any> } = {};
    client: MongoClient;
    db: Db;

    public async connect(callback: any): Promise<void> {

        this.on('error', this.onError);
        this.on('disconnected', this.onDisconnect);
        this.once('open', callback);

        await this.setUpConnection(config.mongo.uri, config.mongo.database, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            autoReconnect: false
        });
    }

    private async setUpConnection(uri: string, database: string, options: any = null): Promise<void> {
        if (this.db === undefined) {
            this.client = new MongoClient(uri, options);
            try {
                await this.client.connect();
                this.db = this.client.db(database);
                this.setEventListeners();
            } catch (error) {
                this.executeCallbacks('error', [ error ]);
            }
        }
        this.executeCallback(this.callbacks['open'], [ this.db ]);
    }

    private on(event: string, callback: any): this {
        if (!Array.isArray(this.callbacks[event])) {
            this.callbacks[event] = [];
        }
        this.callbacks[event].push(callback);

        return this;
    }

    private once(event: string, callback: any): this {
        this.callbacks[event] = callback;

        return this;
    }

    private setEventListeners(): void {
        this.db.on('error', (err): void => {
            this.executeCallbacks('error', [ err ]);
        });

        this.db.on('timeout', (err): void => {
            this.executeCallbacks('error', [ err ]);
        });

        this.db.on('parseError', (err): void => {
            this.executeCallbacks('error', [ err ]);
        });

        this.db.on('close', (err): void => {
            this.executeCallbacks('disconnected', [ err ]);
        });

    }

    public isConnected(): boolean {
        return this.client.isConnected();
    }

    public getConnection(): Db {
        return this.db;
    }

    private onError(error: Error): never {
        console.log(`An error occurred onError ${error}`);
        process.exit();
    }

    private onDisconnect(error: Error): never {
        console.log(`An error occurred onDisconnect ${error}`);
        process.exit();
    }

    private executeCallbacks(event: string, params: Array<any> = []): void {
        if (Array.isArray(this.callbacks[event])) {
            this.callbacks[event].map((callback: any): void => {
                this.executeCallback(callback, params);
            })
        }
    }

    private executeCallback(callback: any, params: Array<any> = []): void {
        if (callback !== undefined) {
            // eslint-disable-next-line prefer-spread
            callback.apply(undefined, params);
        }
    }
}


const connection = new MongoDBConnection();


export default connection;
